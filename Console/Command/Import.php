<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\ImportProductsImages\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Import extends Command
{

    const SUBFOLDER = "images";
    const REMOVE = "remove";

    /**
     * Import constructor.
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directorylist
     * @param \Magento\Framework\Filesystem\Io\File $file
     * @param \Kowal\ImportProductsImages\Lib\Odczyt $odczyt
     * @param \Kowal\ImportProductsImages\Lib\Zapis $zapis
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\App\Filesystem\DirectoryList $directorylist,
        \Magento\Framework\Filesystem\Io\File $file,
        \Kowal\ImportProductsImages\Lib\Odczyt $odczyt,
        \Kowal\ImportProductsImages\Lib\Zapis $zapis,
        \Magento\Framework\App\State $state
    )
    {
        $this->directory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->directorylist = $directorylist;
        $this->file = $file;
        $this->odczyt = $odczyt;
        $this->zapis = $zapis;
        $this->state = $state;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML); // or \Magento\Framework\App\Area::AREA_ADMINHTML
        $main_folder = $input->getArgument(self::SUBFOLDER);
        $option = $input->getOption(self::REMOVE);
        $import_folder = $this->directorylist->getPath('media') . DIRECTORY_SEPARATOR . "import" . DIRECTORY_SEPARATOR . "images";
        $this->file->mkdir($import_folder, 0775);
        $folders = $this->odczyt->getDirContents($import_folder);

        foreach ($folders as $folder => $images) {
            $folder_name = trim(explode("#", pathinfo($folder)['filename'])[0]);
            $output->writeln(print_r($folder_name, true));
            $this->zapis->saveGallery($folder_name, $images);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_importproductsimages:import");
        $this->setDescription("Import zdjęć");
        $this->setDefinition([
            new InputArgument(self::SUBFOLDER, InputArgument::OPTIONAL, "Nazwa podfolderu w którym zamieszczono foldery ze zdjęciami poszczególnych produktów. Domyślna wartość 'images'"),
            new InputOption(self::REMOVE, "-r", InputOption::VALUE_NONE, "Usuń aktualną galerię produktu. Domyślna wartość 'false")
        ]);
        parent::configure();
    }
}

