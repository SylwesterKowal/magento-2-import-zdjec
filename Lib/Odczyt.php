<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 07/12/2020
 * Time: 14:23
 */

namespace Kowal\ImportProductsImages\Lib;


class Odczyt
{
    public function getDirContents($dir, &$results = array())
    {
        $files = scandir($dir);

        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[$dir][] = $path;
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
            }
        }

        return $results;
    }
}
