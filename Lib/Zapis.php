<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 07/12/2020
 * Time: 19:23
 */

namespace Kowal\ImportProductsImages\Lib;


class Zapis
{
    /**
     * @var bool
     */
    protected $removeCurrentGallery = false;

    /**
     * Zapis constructor.
     * @param \Magento\Catalog\Model\Product\Gallery\ReadHandler $readHandler
     * @param \Magento\Catalog\Model\Product\Gallery\Processor $processor
     * @param \Magento\Catalog\Model\ResourceModel\Product\Gallery $gallery
     * @param \Magento\Catalog\Model\Product $productModel
     */
    public function __construct(
        \Magento\Catalog\Model\Product\Gallery\ReadHandler $readHandler,
        \Magento\Catalog\Model\Product\Gallery\Processor $processor,
        \Magento\Catalog\Model\ResourceModel\Product\Gallery $gallery,
        \Magento\Catalog\Model\Product $productModel
    )
    {
        $this->readHandler = $readHandler;
        $this->processor = $processor;
        $this->gallery = $gallery;
        $this->productModel = $productModel;
    }

    public function setRemoveCurrentGallery($v)
    {
        $this->removeCurrentGallery = (boolean)$v;
        return $this;
    }

    public function saveGallery($sku, $images)
    {

        foreach ($images as $image) {

            try {
                $product = $this->productModel->loadByAttribute('sku', $sku);
                if ($product) {
                    $this->readHandler->execute($product);

                    if ($this->removeCurrentGallery == true) {
                        $images = $product->getMediaGalleryImages();
                        foreach ($images as $child) {
                            $this->gallery->deleteGallery($child->getValueId());
                            $this->processor->removeImage($product, $child->getFile());
                        }
                    }

                    $image_type = (strpos($image, '+') !== false) ? ['image', 'small_image', 'thumbnail'] : [];
                    $product->addImageToMediaGallery($image, $image_type, false, false);
                    $product->setStoreId(0);
                    $product->save();
                }
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }
}
